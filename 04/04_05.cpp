#include <vector>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

typedef vector<string>::size_type vec_size;

istream& read(istream& in, vector<string>& dic) {
   if (in) {
      dic.clear();
      string word;
      while(in >> word)
	 dic.push_back(word);
      in.clear();
   }
   return in;
}

void number_words(const vector<string>& dic) {
   for (vec_size i = 0; i < dic.size(); i++)
      cout << dic[i] << "\t" << dic[i].size() << endl;
}

void find_words(const vector<string>& dic) {
   for (vec_size i = 0; i < dic.size(); i++) {
      string w = dic[i];
      int times = 0;
      for (vec_size j = 0; j < dic.size(); j++) {
	 if (j == i) continue;
	 if (dic[i] == dic[j])
	    times++;
      }
      cout << dic[i] << "\t" << times << " times";
   }
      
}

int main() {
   vector<string> dic;
   while(read(cin, dic));
   number_words(dic);
   find_words(dic);

   return 0;
}
