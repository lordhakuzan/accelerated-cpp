#include <iostream>
#include <vector>
#include <iomanip>

using std::max;
using std::setw;
using std::cout;
using std::endl;

int main() {
   for(int i = 1; i < 101; i++)
      cout << setw(3) << i << " " << setw(5) << i*i << endl;
}
