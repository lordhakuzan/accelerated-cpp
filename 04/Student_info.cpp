#include "Student_info.h"

using std::istream;
using std::vector;
using std::string;
using std::cout;

bool compare(const Student_info& x, const Student_info& y) {
   return x.name < y.name;
}

istream& read(std::istream& is, Student_info& s) {
   cout << "Please enter Student first name: ";
   string name;
   is >> s.name;

   cout << "Please enter your midterm and final exam grades: ";
   is >> s.name >> s.midterm >> s.final;

   read_hw(is, s.homework);
   return is;
}

istream& read_hw(istream& in, vector<double>& hw) {
   if (in) {
      hw.clear();

      double x;
      while (in >> x)
	 hw.push_back(x);

      in.clear();
   }
   return in;
}
