#include <iostream>
#include <vector>
#include "Student_info.h"

using std::max;

int main() {

   std::string::size_type maxlen =  10;
   Student_info s;
   s.name = "Edgar";
   max(s.name.size(), maxlen);
   return 0;
}
