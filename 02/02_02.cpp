#include <iostream>
#include <string>

using std::cin;  using std::endl;
using std::cout; using std::string;

int main() {
	cout << "Please enter your first name:";

	string name;
	cin >> name;

	const string greeting = "Hello, " + name + "!";

	const int padRows = 0;
	const int padCols = 2;

	const int rows = padRows * 2 + 3;
	const string::size_type cols = greeting.size() + padCols * 2 + 2;

	cout << endl;

	for (int r = 0; r != rows; r++) {
		string::size_type c = 0;

		while (c != cols) {
			if (r == padRows + 1 && c == padCols + 1) {
				cout << greeting;
				c += greeting.size();
			} else {
				if (r == 0 || r == rows - 1 ||
					c == 0 || c == cols - 1)
					cout << "*";
				else
					cout << " ";
				++c;
			}
		}
		cout << endl;
	}
	return 0;
}
