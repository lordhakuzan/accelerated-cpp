#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
using std::string;
using std::endl;
using std::vector;

int main() {
	cout << "Type your words: ";
	string word;
	vector<int> words;
	while(cin >> word)
		words.push_back(word.size());

	sort(words.begin(), words.end());

	cout << "Shortest: " << words[0] << ", Longest: " << words[words.size() - 1] << endl;
	
}
