#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ios>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::streamsize;
using std::setprecision;
using std::sort;

int main()
{
   string name;
   vector<string> students;
   vector<double> grades;
   int students_number;
   double midterm, final;
   double median;
   double homework1, homework2;

   cout << "How many students are: ";
   cin >> students_number;

   for (int i = 0; i < students_number; i++) {
      cout << i + 1 << ") Name: ";
      cin >> name;
      students.push_back(name);
          
      cout << "Please enter your midterm and final exam grades: ";
      cin >> midterm >> final;

      cout <<  "Enter your 2 homework grades."
	       " Followed by end-of-line: ";
      cin >> homework1 >> homework2;
      median = (homework1 + homework2) / 2;

      double final_grade = .4 * final + .2 * midterm + .4 * median;

      grades.push_back(final_grade);
   }

   streamsize prec = cout.precision(3);
   for (int i = 0; i < students_number; i++) {
      cout << students[i] << " " << grades[i] << endl;
   }
   setprecision(prec);

   return 0;
}
