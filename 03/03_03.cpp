#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::endl;

int main()
{
	cout << "Write the words: ";
	string word;
	vector<string> words;
	while (cin >> word)
		words.push_back(word);	

	sort(words.begin(), words.end());
	int counter = 1;	
	string j = words[0];
	cout << "after sorting" << endl;
	for(vector<string>::size_type i = 1; i < words.size(); i++) {
		if (j == words[i]) {
			counter++;
		} else {
			cout << j << " : " << counter << endl;
			j = words[i];
			counter = 1;
		}
	}
	return 0;	
}
